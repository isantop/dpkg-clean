# dpkg-clean

A simple cleanup tool for leftover dpkg configs.

## Description

Even with `apt purge`, dependent packages aren't fully purged most of the time and can leave behind unnecessary config files. This script is a python wrapper for `python-apt` that polls for orphaned configs and presents you with a simple menu allowing you to remove (or keep) them at leisure.

## Usage

`dpkg-clean` is an incredibly simple utility, but it does have a few options. Basic use can be accomplished by simply running `dpkg-clean`.

This will poll for orphaned configs, display a multicheck list of any found configs, and allow you to select which ones you want to remove.

* `-a`, `--all`:

The `--all` flag bypasses the check list and assumes you want to remove all found configs. Note that you will still be prompted for verification before any changes are made.

* `-n`, `--no-check`:

The `--no-check` flag bypasses the verification and immediately makes any requested changes to the package database.
